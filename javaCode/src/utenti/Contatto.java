package utenti;

/**
 * Created by mac on 25/05/16.
 */
public class Contatto {

    private String nome;
    private String cognome;
    private String email;

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void visualizzaContatto(Contatto contatto){

        System.out.println("Nome: " + contatto.getNome());
        System.out.println("Cognome: " + contatto.getCognome());
        System.out.println("e-mail: " + contatto.getEmail());

    }
}
