package utenti;
import java.util.*;

import sistemaCentrale.Progetto;


public class Amministratore extends Utente {

	ArrayList<Progetto> progettiAvviati;

	public void visualizzaProgettiAvviati(){
        for(Progetto i: progettiAvviati){

            System.out.println("Nome progetto: " + i.getNome());
            System.out.println("Data scadenza: " + i.getDataScadenza() + "\n" );

        }
    }

}
