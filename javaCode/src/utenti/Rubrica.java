package utenti;

import java.util.*;

public class Rubrica {

	static Rubrica uniqueInstance;
	private Utente proprietario;
	private ArrayList<Contatto> contactList;
	
	protected Rubrica(){
		//Exists only to defeat instantiation	
	}
	
	public static Rubrica getInstance(){
		
		if (uniqueInstance == null)
			uniqueInstance = new Rubrica();
		return uniqueInstance;
	}
	

	public void visualizzaContatti(){
		
		System.out.println("Elenco contatti in Rubrica: \n");
		
		for (int i = 0; i < contactList.size(); i++){
			
			System.out.println("Nome: " + contactList.get(i).getNome() + "\n");
			System.out.println("Cognome: " + contactList.get(i).getCognome() + "\n");
			System.out.println("Tel 1: " + contactList.get(i).getTelefono1() + "\n");
			System.out.println("E-mail: " + contactList.get(i).getEmail() + "\n");
			
		}//exit for		
	}//exit visualizzaContatti()
	
	public void gestisciContatti(){}

	public Utente getProprietario() {
		return proprietario;
	}

	public void setProprietario(Utente proprietario) {
		this.proprietario = proprietario;
	}

	public ArrayList<Contatto> getContactList() {
		return contactList;
	}

	public void setContactList(ArrayList<Contatto> contactList) {
		this.contactList = contactList;
	}
	

	
}
