package sistemaCentrale;

import utenti.Amministratore;
import utenti.Utente;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by giuseppetesta on 25/05/16.
 */
public class Team implements Serializable {

    public Team() {

    }

    //private Progetto teamProject;
    private Amministratore admin;
    private ArrayList<Utente> team = new ArrayList<Utente>();

    public ArrayList<Utente> getTeam() {
        return team;
    }

    public void setTeam(ArrayList<Utente> team) {
        this.team = team;
    }

    public Amministratore getAdmin() {
        return admin;
    }

    public void setAdmin(Amministratore admin) {
        this.admin = admin;
    }
}
