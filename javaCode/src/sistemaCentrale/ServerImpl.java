package sistemaCentrale;

import common.*;
import common.Utente;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by giuseppetesta on 27/05/16.
 */

public class ServerImpl extends UnicastRemoteObject implements Server, ProgettoInterface/*, TaskInterface, TeamInterface*/{

    private ArrayList<Utente> utenti;
    private ArrayList<Progetto> progetti;

    private String password = "tetro";
    private String userName = "tetro";


    /** Singleton */
    private static ServerImpl instance;

    public ServerImpl()throws RemoteException {
        super();
        this.utenti = new ArrayList<common.Utente>();
        this.progetti = new ArrayList<Progetto>();
    }

    public static synchronized ServerImpl getInstance()
    {
        if (instance == null)
            try {
                instance = new ServerImpl();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        return instance;
    }

    /** Methods Server */
    public void registraUtente(Utente utente) throws RemoteException {
        if( !utenti.contains(utente) ) {
            utenti.add(utente);
            //System.out.println("Utente "+ utente.getNome() + " logged");
        }
    }

    @Override
    public void unregisterClient(Utente client) throws RemoteException {
        if( !utenti.contains(client) ) {
            utenti.remove(client);
            //System.out.println("Utente "+ utente.getNome() + " logged");
        }
    }

    @Override
    public ArrayList<Progetto> getProgetti() throws RemoteException {
        return progetti;
    }

    @Override
    public void addProgetto(Progetto progetto) throws RemoteException {
        if (!progetti.contains(progetto)) {
            progetti.add(progetto);
            System.out.println("Progetto " + progetto.getNome() + " aggiunto");
        }
    }

    @Override
    public void visualizzaProgetto(Progetto p) throws RemoteException {

        System.out.println("\nNome progetto: "+p.getNome());
        System.out.println("Admin progetto: "+p.getAmministratore().getNome());
        System.out.println("Data creazione progetto: "+p.getDataCreazione());
        System.out.println("Data scadenza progetto: "+p.getDataScadenza());
        System.out.println("Stato progetto: "+p.getStatoProgetto()+"\n\n");
    }

    @Override
    public void stampaUtentOnline() throws RemoteException {
        System.out.print("Utenti online:\n");
        int count1 = 0;
        for(Utente u:utenti){
            if (u.isLogin() == true) {
                System.out.println(u.getNome());
                count1++;
            }
        }
        if(count1==0){ System.out.print("Nessun utente attivo\n"); }
    }

    @Override
    public void stampaProgettiAttivi() {
        System.out.print("Progetti attivi:\n");
        int count2 = 0;
        for (Progetto p:progetti) {
            if (p.getStatoProgetto() == Progetto.StatoProgetto.ATTIVO) {
                System.out.println(p.getNome());
                count2++;
            }
        }
            if (count2 == 0) { System.out.print("Nessun progetto attivo\n"); }
    }



    @Override
    public void stampaProgettiConclusi() throws RemoteException {
        System.out.print("Progetti conclusi:\n");
        int count3 = 0;
        for (Progetto p : progetti) {
            if (p.getStatoProgetto() == Progetto.StatoProgetto.CHIUSO) {
                System.out.println(p.getNome());
                count3++;
            }

        }
        if (count3 == 0) { System.out.print("Nessun progetto concluso\n"); }
    }

    /** Methods ProgettoInterface */
    @Override
    public void modificaNome(Progetto myProject, String newName) throws RemoteException {
        myProject.setNome(newName);
    }

    @Override
    public void modificaScadenza(Progetto myProject, Date newDate) throws RemoteException {
        myProject.setDataScadenza(newDate);
    }

    @Override
    public void modificaStatoPorgetto(Progetto p, Progetto.StatoProgetto newStato) throws RemoteException { p.setStatoProgetto(newStato); }

    @Override
    public void chiudiProgetto(Progetto myProject) throws RemoteException { //in generale si deve modificare lo stato del progetto
        myProject.setStatoProgetto(Progetto.StatoProgetto.CHIUSO);
    }

/*
    /Methods TaskInterface
    @Override
    public void addTask(Task task, Progetto progetto) throws RemoteException {

        progetto.getToDoList().add(task);

    }
    @Override
    public void removeTask(Progetto progetto, int pos) throws RemoteException {

        progetto.getToDoList().remove(pos);

    }
    @Override
    public void visualizzaTask(Progetto p) throws RemoteException {

        for(Task tmp:p.getToDoList()) {
            System.out.println(tmp.getName());
            System.out.println(tmp.getStatoTask());
        }

    }  //per ora stampa solo la lista dei nomi delle task e lo stato

    @Override
    public void addResponsabile(Progetto p, Task t, Responsabile r) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).getListaResopnabili().add(r);
    }
    @Override
    public void removeResponsabile(Progetto p, Task t, Responsabile r) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).getListaResopnabili().remove(r);
    }
    @Override
    public void modificaNomeTask(Progetto p, Task t, String newName) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setName(newName);
    }
    @Override
    public void modificaDescrizioneTask(Progetto p, Task t, String newDescrizione) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setDescrizione(newDescrizione);
    }
    @Override
    public void modificaLuogoTask(Progetto p , Task t, String newLuogo) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setLuogo(newLuogo);
    }
    @Override
    public void modificaDeadlineTask(Progetto p , Task t, Date newDeadline) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setDeadline(newDeadline);
    }
    @Override
    public void modificaStatoTask(Progetto p, Task t, Task.StatoTask newStato) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setStatoTask(newStato);
    }
    @Override
    public void modificaOrdineTask(Progetto p, Task t, int ordine) throws RemoteException {
        int pos = posizioneTask(p,t);
        p.getToDoList().get(pos).setOrderNumber(ordine);
    }

    //METDHODS TEAM

    public void addMember(Progetto myProg, Utente utente) throws RemoteException {

        myProg.getTeam().getTeam().add(utente);

    }

    public void removeMemeber(Progetto myProg, Utente utente) throws RemoteException {

        myProg.getTeam().getTeam().remove(utente);

    }

    @Override
    public void addMember(Progetto myProg, utenti.Utente utente) throws RemoteException {

    }

    @Override
    public void removeMemeber(Progetto myProg, utenti.Utente utente) throws RemoteException {

    }

    public Team getTeam(Progetto myProg) throws RemoteException {

        return myProg.getTeam();
    }*/

    //util

    public int posizioneTask(Progetto p, Task t){

        if(p.getToDoList().contains(t)){
            return p.getToDoList().indexOf(t);
        }else{
            System.out.println("Warning: la task non è presente nel progetto!");
            return -1;
        }

    }

}
