package sistemaCentrale;

import common.MyFactory;
import common.ProgettoInterface;
import common.Utente;
import sistemaCentrale.Progetto;
import sistemaCentrale.Task;
import sistemaCentrale.Team;
import utenti.Amministratore;
import utenti.Responsabile;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by giuseppetesta on 27/05/16.
 */
public class MyFactroyImpl extends UnicastRemoteObject implements MyFactory {

    public MyFactroyImpl() throws RemoteException {
        super();
    }

    // Factory Progetto

    @Override
    public Progetto creaProgetto(Utente utente, String nome, String descrizione) throws RemoteException {

        Progetto myProgetto = new Progetto();
        myProgetto.setDescrizione(descrizione);
        myProgetto.setNome(nome);
        myProgetto.setAmministratore(utente);

        return myProgetto;
    }


    //Factroy Task
    public Task creaTask(ArrayList<Utente> responsabili, String nome, String descrizione, String luogo) {

        Task task = new Task();
        task.getListaResopnabili().addAll(responsabili);
        task.setName(nome);
        task.setDescrizione(descrizione);
        task.setDeadline(null);
        task.setLuogo(luogo);

        return task;

    }

}