package sistemaCentrale.ServerGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mac on 14/06/16.
 */
public class FrameAccessoServer extends JFrame{


    private static final String titolo = "TeTro - Social Planner [Server]";

    //Pannello Nord
    private static final JLabel inserisciDati = new JLabel("Inserisci username e password per accedere al server: ");
    private static final JPanel NordPanel = new JPanel();

    //Pannello Centrale
    private static final JLabel usernameLabel = new JLabel("Username:");
    private static final JTextField campoUsername = new JTextField();
    private static final JLabel passwordLabel = new JLabel("Password:");
    private static final JPasswordField campoPassword = new JPasswordField();
    private static final JPanel CentroPanel = new JPanel();

    //Pannello Sud
    private static final JButton annullaButton = new JButton("Annulla");
    private static final JButton okButton = new JButton("OK");
    private static final JPanel SudPanel = new JPanel();
/*

        public FrameAccessoServer(){

            super(titolo);
            this.setDefaultCloseOperation(EXIT_ON_CLOSE);

            Container contentPane = this.getContentPane();

            //Pannello Nord
            NordPanel.add(inserisciDati);
            contentPane.add(NordPanel, BorderLayout.NORTH);

            //Pannello Centro
            CentroPanel.setLayout(new GridLayout(2,2));
            //campoUsername.addActionListener(new ClientFrameListener());
            //campoPassword.addActionListener(new ClientFrameListener());
            CentroPanel.add(usernameLabel);
            CentroPanel.add(campoUsername);
            CentroPanel.add(passwordLabel);
            CentroPanel.add(campoPassword);
            contentPane.add(CentroPanel, BorderLayout.CENTER);

            //Pannello Sud
            SudPanel.setLayout(new FlowLayout());
            SudPanel.add(okButton);
            okButton.addActionListener(new ClientFrameListener());
            SudPanel.add(annullaButton);
            annullaButton.addActionListener(new ClientFrameListener());
            contentPane.add(SudPanel, BorderLayout.SOUTH);


            this.pack();
            this.setLocationRelativeTo(null); //posiziona la finestra al centro dello schermo
            this.setVisible(true);
        }*/

    public static JButton getAnnullaButton() {
        return annullaButton;
    }

    public static JButton getOkButton() {
        return okButton;
    }
}

