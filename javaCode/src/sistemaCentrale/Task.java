package sistemaCentrale;

import java.io.Serializable;
import java.util.*;

import common.Utente;
import utenti.Responsabile;

public class Task implements Serializable{
	
	public enum StatoTask{		
		IN_CORSO,
		TERMINATA,
		NON_TERMINATA;
		}

	private String name;
	private String descrizione;
	private String luogo;
	private Date dataAggiunta = new Date();
	private Date deadline;
	private StatoTask statoTask;
	private int orderNumber;
	private ArrayList<Utente> listaResopnabili = new ArrayList<Utente>();


	//costruttore
	@SuppressWarnings("deprecation")
	public Task(){

		Calendar cal = Calendar.getInstance();
		this.dataAggiunta.setDate(cal.get(Calendar.DATE));
		this.statoTask = StatoTask.IN_CORSO;
	}
	
	//getter e setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getLuogo() {
		return luogo;
	}
	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public Date getDataAggiunta() {
		return dataAggiunta;
	}

	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public ArrayList<Utente> getListaResopnabili() {
		return listaResopnabili;
	}
	public void setListaResopnabili(ArrayList<Utente> listaResopnabili) {
		this.listaResopnabili = listaResopnabili;
	}

	public StatoTask getStatoTask() {
		return statoTask;
	}
	public void setStatoTask(StatoTask statoTask) {
		this.statoTask = statoTask;
	}

	
}
