package sistemaCentrale;

import common.MyFactory;
import common.ProgettoInterface;
import common.Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * Created by giuseppetesta on 04/06/16.
 */
public class SistemaServer {

    private static final long serialVersionUID = 8553201216151204858L;

    /** Run with:
     -Djava.security.policy=server.policy
     -Djava.rmi.server.codebase="file://${workspace_loc:/SistemaServer}/bin/ file://${workspace_loc:/Common}/bin/"
     */

    public static void main(String[] args) throws RemoteException {
        /*
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }*/ //???

        try {

            /*int i = 0;
            boolean t = false;
            while( i < 4 || t == true){
                Scanner userName = new Scanner(System.in);
                System.out.println("User: ");
                String nome = userName.nextLine();
            }*/

            /* create and start registry*/
            Registry registry = LocateRegistry.createRegistry( 12345 );

            Server server = new ServerImpl();
            registry.rebind("srv", server);

            ProgettoInterface progetto = new ServerImpl();
            registry.rebind( "project",  progetto);

            MyFactory factory = new MyFactroyImpl();
            registry.rebind("factory", factory);

            System.out.println("SistemaServer online...\n");

            boolean t = true;
            while( t == true) {

                System.out.println("\n--------------- Comandi: ---------------\n"
                        + "\n • Inserisci 1 per visualizzare gli utetnti online\n"
                        + "\n • Inserisci 2 per visualizzare i porgetti attivi\n"
                        + "\n • Inserisci 3 per visualizzare i porgetti conclusi\n"
                        //+ "\n • Inserisci 0 per uscire!\n\n"
                        );

                Scanner userIn = new Scanner(System.in);

                if (userIn.hasNextInt()) {

                    switch (userIn.nextInt()) {

                        case 1: // visualizza gli utenti online
                            Scanner u = new Scanner(System.in);
                            String ut = u.nextLine();
                            server.stampaUtentOnline();
                            break;
                        case 2: // visualizza i progetti attivi
                            Scanner  p = new Scanner(System.in);
                            String pa = p.nextLine();
                            server.stampaProgettiAttivi();
                            break;
                        case 3: // visualizza i progetti coclusi
                            Scanner  pc = new Scanner(System.in);
                            String in = pc.nextLine();
                            server.stampaProgettiConclusi();
                            break;
/*
                        case 0: //exit
                            System.out.println("\n**************** bye bye ****************");
                            t = false;
                            break;
*/
                        default:
                            System.out.println("\n**** COMANDO NON VALIDO ****");
                            break;
                    }
                }
            }
        } catch (RemoteException e) {
            System.err.println("SistemaServer exception. ");
            e.printStackTrace();
        }
    }
}
