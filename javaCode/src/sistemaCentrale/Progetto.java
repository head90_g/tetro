package sistemaCentrale;

import java.io.Serializable;
import java.util.*;

import common.Utente;
import utenti.Amministratore;

/**
 * <p><b>Classe</b> per la gestione di un singolo progetto</p>
 *
 * @author giuseppetesta
 */
public class Progetto implements Serializable /*ProgettoInterface*/{

	public enum StatoProgetto{
		ATTIVO,
		CHIUSO;
		}

	private String nome;
	private String descrizione;

	private Date dataCreazione = new Date();
	private Date dataScadenza;

	private Utente amministratore;

	//private Amministratore projectLeader;
	private ArrayList<Task> toDoList = new ArrayList<Task>();
	//private ArrayList<Utente> team = new ArrayList<Utente>();

	//private Team team = new Team();
	private StatoProgetto statoProgetto;
	
	//costruttore
	/*public Progetto(String nome, Date dataCreazione, Date dataScadenza, Amministratore projectLeader, ArrayList<Task> toDoList, ArrayList<UtenteImpl> team,
			Stato statoProgetto){
		this.nome = nome;
		this.dataCreazione = dataCreazione;
		this.dataScadenza = dataScadenza;
		this.projectLeader = "projectLeader";
		this.team = team;
		this.toDoList = toDoList;
		this.statoProgetto = Stato.ATTIVO;
	}*/

	/*@Override
	public TaskIteratorImpl creaIterator() {
		TaskIteratorImpl task = new TaskIteratorImpl();
		return task;
	}*/


	@SuppressWarnings("deprecation")
	public Progetto(){

		Calendar cal = Calendar.getInstance();
		this.dataCreazione.setDate(cal.get(Calendar.DATE));
		this.statoProgetto = StatoProgetto.ATTIVO;
	}
	
	// getter e setter
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public Date getDataCreazione() {
		return dataCreazione;
	}

	public Date getDataScadenza() {
		return dataScadenza;
	}
	public void setDataScadenza(Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	public ArrayList<Task> getToDoList() {
		return toDoList;
	}
	public void setToDoList(ArrayList<Task> toDoList) {
		this.toDoList = toDoList;
	}

	public StatoProgetto getStatoProgetto() {
		return statoProgetto;
	}
	public void setStatoProgetto(StatoProgetto statoProgetto) {
		this.statoProgetto = statoProgetto;
	}

	public Utente getAmministratore() {
		return amministratore;
	}
	public void setAmministratore(Utente amministratore) {
		this.amministratore = amministratore;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}



	/*
	public ArrayList<Utente> getTeam() {
		return team;
	}
	public void setTeam(ArrayList<Utente> team) {
		this.team = team;
	}

	public Team getTeam() {
		return team;
	}
	public void setTeam(Team team) {
		this.team = team;
	}
	public void setProjectLeader(Amministratore projectLeader) {
		this.projectLeader = projectLeader;
	}
	public Amministratore getProjectLeader() {
		return projectLeader;
	}
	*/

}

 
