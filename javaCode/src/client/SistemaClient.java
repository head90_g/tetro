package client;

import common.MyFactory;
import common.ProgettoInterface;
import common.Server;
import common.Utente;
import sistemaCentrale.Progetto;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

/**
 * @author Giuseppe Testa
 * 01/06/16
 */
public class SistemaClient {

    /** Run with - Djava.security.policy=client.policy */

    public static void main(String[] args) throws NotBoundException, IOException {

        /*if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }*/ // ???

        try {
            Registry registry = LocateRegistry.getRegistry( 12345 );
            Server srv = (Server) registry.lookup("srv") ;
            MyFactory factory = (MyFactory) registry.lookup("factory");
            //ProgettoInterface project = (ProgettoInterface) registry.lookup("project");
            String nomeClient = "Jack" + (int)(Math.random() * 100);
            Utente client = new UtenteImpl( nomeClient, srv/*, project*/, factory);

            boolean t = true;
            while( t == true) {

                System.out.println("\n   ---"+client.getNome()+"---\n"
                        + "\n • 1 -> Progetti\n"
                        + "\n • 2 -> Attività\n"
                        + "\n • 3 -> Contatti\n"
                        + "\n • 0 -> logout\n\n");

                Scanner userIn = new Scanner(System.in);
                if (userIn.hasNextInt()) {
                    switch (userIn.nextInt()) {

                        case 1: // progetti

                            boolean t1 = true;
                            while( t1 == true) {

                                System.out.println("\n--------------- Progetti: ---------------\n"
                                        + "\n • 1 crea nuovo progetto\n"
                                        + "\n • 2 porgetti attivi\n"
                                        + "\n • 3 porgetti conclusi\n"
                                        + "\n • 0 per tornare indietro\n\n");

                                Scanner userIn2 = new Scanner(System.in);
                                if (userIn2.hasNextInt()) {
                                    switch (userIn2.nextInt()) {

                                        case 1: //nuovo progetto

                                            Scanner userIn3 = new Scanner(System.in);
                                            System.out.println("\ninserisci il nome del progetto:");
                                            if (userIn3.hasNext()) {
                                                String nome = userIn3.nextLine();
                                                System.out.println("\ninserisci la descrizione del progetto:");
                                                String descrizione = userIn3.nextLine();
                                                ProgettoInterface project = (ProgettoInterface) registry.lookup("project");
                                                Progetto progetto = factory.creaProgetto(client, nome, descrizione);
                                                client.addProgetto(progetto);

                                                client.gestisciProgetto(progetto, client); // facendo così modifico anche il progetto nel server??
                                            }
                                            break;

                                        case 2: // progetti attivi

                                            for (Progetto p : client.getProjects()) {
                                                if (p.getStatoProgetto() == Progetto.StatoProgetto.ATTIVO) {
                                                    client.visualizzaProgetto(p);
                                                }
                                            }
                                            break;
                                        case 3: // progetti chiusi

                                            for (Progetto p : client.getProjects()) {
                                                if (p.getStatoProgetto() == Progetto.StatoProgetto.CHIUSO) {
                                                    client.visualizzaProgetto(p);
                                                }
                                            }
                                            break;

                                        case 0: //back

                                            t1 = false;
                                            break;

                                        default:
                                            System.out.println("\n**** COMANDO NON VALIDO ****");
                                            break;
                                    }
                                }

                            }

                            break;

                        case 2: //

                            boolean t3 = true;
                            while( t3 == true) {

                                System.out.println("\n--------------- Attività----------\n"
                                        + "\n • 1 attività da svolgere\n"
                                        + "\n • 2 attività completate\n"
                                        + "\n • Inserisci 0 per tornare indietro\n\n");

                                Scanner userIn2 = new Scanner(System.in);
                                if (userIn2.hasNextInt()) {
                                    switch (userIn2.nextInt()) {
                                        case 1: //

                                            break;

                                        case 2:

                                        case 0: //back

                                            t3 = false;
                                            break;

                                        default:
                                            System.out.println("\n**** COMANDO NON VALIDO ****");
                                            break;
                                    }
                                }

                            }
                            break;

                        case 3: // contatti

                            boolean t4 = true;
                            while( t4 == true) {

                                System.out.println("\n--------------- Contatti----------\n"
                                        + "\n • 1 Aggiungi contatto\n"
                                        + "\n • Inserisci 0 per tornare indietro\n\n");

                                Scanner userIn2 = new Scanner(System.in);
                                if (userIn2.hasNextInt()) {
                                    switch (userIn2.nextInt()) {
                                        case 1: // aggiungi contatto
                                            System.out.println("\n**** Work in porgress... ****");

                                            break;

                                        case 0: //back

                                            t4 = false;
                                            break;

                                        default:
                                            System.out.println("\n**** COMANDO NON VALIDO ****");
                                            break;
                                    }
                                }

                            }
                            break;

                        case 0: // logout

                            srv.unregisterClient(client);
                            UnicastRemoteObject.unexportObject(client, false);
                            System.out.println("Session ended");
                            t = false;

                            break;

                        default:
                            System.out.println("\n**** COMANDO NON VALIDO ****");
                            break;
                    }
                }
            }

            /*Scanner s = new Scanner(System.in);
            while(true){
                System.out.println("enter the name of the project: \npress e to exit");
                String message = s.nextLine();
                if (message.equals("e")){
                    srv.unregisterClient(client);
                    UnicastRemoteObject.unexportObject(client, false);
                    System.out.println("Session ended");
                    break;


                } else {

                    ProgettoInterface project = (ProgettoInterface) registry.lookup("project"); //che succede la seconda volta??
                    srv.addProgetto(factory.creaProgetto(client, message));
                }
            }*/
        } catch (RemoteException e){
            System.err.println("SistemaClient exception: ");
            e.printStackTrace();
        }
    }
}