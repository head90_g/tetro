package client;

import common.MyFactory;
import common.Server;
import common.Utente;
import sistemaCentrale.Progetto;
import sistemaCentrale.Task;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 * @author Giuseppe Testa
 *         06/06/16
 */
public class UtenteImpl extends UnicastRemoteObject implements Utente{


    protected final String nome;
    protected boolean login;
    protected final Server srv;
    protected final ArrayList<Progetto> projects = new ArrayList<Progetto>();
    protected final MyFactory factory;

    protected UtenteImpl(String nome,  Server srv/*, ProgettoInterface project*/, MyFactory factory) throws RemoteException {

        //this.project.add(project);
        this.nome = nome;
        this.login = true;
        this.factory = factory;
        this.srv = srv;

        try {
            srv.registraUtente(this);
            System.out.println(this.getNome() + " logged\n");
        } catch (RemoteException e){
            System.out.println("Impossibile loggarsi");
        }
    }

    @Override
    public void login() {

    }

    @Override
    public void logout() {

    }

    public boolean isLogin() {
        return login;
    }

    public boolean setLogin(boolean log){
        login = log;
        return log;
    }

    @Override
    public void addTask(Progetto progetto, Task task) throws RemoteException {
        if (projects.contains(progetto)) {
            projects.get(projects.indexOf(progetto)).getToDoList().add(task);
            System.out.println("Task " + task.getName() +" "+task.getDescrizione()+" "+task.getLuogo()+"  aggiunta al progetto " + progetto.getNome());
        }
    }

    @Override
    public void addProgetto(Progetto progetto) throws RemoteException {
        if (!projects.contains(progetto)) {
            projects.add(progetto);
            srv.addProgetto(progetto);
            System.out.println("Progetto " + progetto.getNome() + " aggiunto");
        }
    }

    @Override
    public void gestisciProgetto(Progetto progetto, Utente client) throws RemoteException {

        boolean temp = true;
        while (temp == true) {

            System.out.println("\n • 1 inserisci task\n"
                             + "\n • 2 inserisci scadenza progetto\n"
                             + "\n • 3 chiudi progetto\n"
                             + "\n • 0 per tornare indietro\n\n");


            Scanner userIn4 = new Scanner(System.in);
            if (userIn4.hasNextInt()) {
                switch (userIn4.nextInt()) {

                    case 1: // inserisci task

                        ArrayList<Utente> responsabili = new ArrayList<Utente>();
                        responsabili.add(client);

                        Scanner userIn5 = new Scanner(System.in);
                        System.out.println("inserisci il nome della task");
                        String nomeTask = userIn5.nextLine();
                        System.out.println("inserisci la descrizione della task");
                        String descrizioneTask = userIn5.nextLine();
                        System.out.println("inserisci il luogo della task");
                        String luogoTask = userIn5.nextLine();

                        int s = 0;
                        while(s == 0) {
                            Scanner userIn6 = new Scanner(System.in);
                            System.out.println("inserisci un responsbile o 0 per uscire\n");
                            if(userIn6.hasNextLine()){
                                String resp = userIn6.nextLine();
                                if(resp.equals("0")) { s=1; break;} else{
                                    System.out.println("Rsponsabile aggiunto\n");
                                    //si deve implementare l'GGIUNTA DEL responsabile
                                }
                            }

                        }

                        client.addTask(progetto, factory.creaTask(responsabili, nomeTask, descrizioneTask, luogoTask));

                        break;

                    case 2:
                        System.out.println("Inserisci la deadline del progetto\n");

                        Scanner userIn7 = new Scanner(System.in);
                        System.out.println("Giorno:\n");
                        int day = userIn7.nextInt();
                        System.out.println("Mese:\n");
                        int month = userIn7.nextInt();
                        System.out.println("Anno:\n");
                        int year = userIn7.nextInt();

                        Date deadline = new Date(year, month, day);
                        progetto.setDataScadenza(deadline);

                        break;

                    case 3:
                        progetto.setStatoProgetto(Progetto.StatoProgetto.CHIUSO);
                        srv.chiudiProgetto(progetto);
                        break;
                    case 0:
                        temp = false;
                        break;

                    default:
                        System.out.println("\n**** COMANDO NON VALIDO ****");
                        break;
                }
            }
        }
    }


    public ArrayList<Progetto> getProjects() {
        return projects;
    }

    @Override
    public void visualizzaProgetto(Progetto p) throws RemoteException {
        System.out.println("\nNome progetto: "+p.getNome());
        System.out.println("Admin progetto: "+p.getAmministratore().getNome());
        System.out.println("Data creazione progetto: "+p.getDataCreazione());
        System.out.println("Data scadenza progetto: "+p.getDataScadenza());
        System.out.println("Stato progetto: "+p.getStatoProgetto()+"\n\n");
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {

    }

}
