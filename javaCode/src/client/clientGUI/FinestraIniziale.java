package client.clientGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mac on 12/07/16.
 */
public class FinestraIniziale extends Container {

    private JButton loginButton;
    private JPanel StartingPanel;
    private JButton registratiButton;


    public JButton getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(JButton loginButton) {
        this.loginButton = loginButton;
    }

    public JPanel getStartingPanel() {
        return StartingPanel;
    }

    public void setStartingPanel(JPanel startingPanel) {
        StartingPanel = startingPanel;
    }

    public JButton getRegistratiButton() {
        return registratiButton;
    }

    public void setRegistratiButton(JButton registratiButton) {
        this.registratiButton = registratiButton;
    }
}

