package client.clientGUI;

import javax.swing.*;

/**
 * Created by mac on 19/07/16.
 */
public class FinestraInviaNotifica {


    private JPanel inviaNotificaPanel;
    private JScrollPane elencoDestinatari;
    private JPanel tipologiaPanel;
    private JPanel destinatariPanel;
    private JPanel buttonPanel;
    private JComboBox tipologiaComboBox;
    private JPanel testoPanel;
    private JTextArea textArea1;
    private JButton inviaButton;
    private JButton annullaButton;
    private JTextPane textPane1;

    public JPanel getInviaNotificaPanel() {
        return inviaNotificaPanel;
    }

    public void setInviaNotificaPanel(JPanel inviaNotificaPanel) {
        this.inviaNotificaPanel = inviaNotificaPanel;
    }

    public JScrollPane getElencoDestinatari() {
        return elencoDestinatari;
    }

    public void setElencoDestinatari(JScrollPane elencoDestinatari) {
        this.elencoDestinatari = elencoDestinatari;
    }

    public JPanel getTipologiaPanel() {
        return tipologiaPanel;
    }

    public void setTipologiaPanel(JPanel tipologiaPanel) {
        this.tipologiaPanel = tipologiaPanel;
    }

    public JPanel getDestinatariPanel() {
        return destinatariPanel;
    }

    public void setDestinatariPanel(JPanel destinatariPanel) {
        this.destinatariPanel = destinatariPanel;
    }

    public JPanel getButtonPanel() {
        return buttonPanel;
    }

    public void setButtonPanel(JPanel buttonPanel) {
        this.buttonPanel = buttonPanel;
    }

    public JComboBox getTipologiaComboBox() {
        return tipologiaComboBox;
    }

    public void setTipologiaComboBox(JComboBox tipologiaComboBox) {
        this.tipologiaComboBox = tipologiaComboBox;
    }

    public JPanel getTestoPanel() {
        return testoPanel;
    }

    public void setTestoPanel(JPanel testoPanel) {
        this.testoPanel = testoPanel;
    }

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(JTextArea textArea1) {
        this.textArea1 = textArea1;
    }

    public JButton getInviaButton() {
        return inviaButton;
    }

    public void setInviaButton(JButton inviaButton) {
        this.inviaButton = inviaButton;
    }

    public JButton getAnnullaButton() {
        return annullaButton;
    }

    public void setAnnullaButton(JButton annullaButton) {
        this.annullaButton = annullaButton;
    }
}
