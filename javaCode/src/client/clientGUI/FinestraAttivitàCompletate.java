package client.clientGUI;

import javax.swing.*;

/**
 * Created by mac on 19/07/16.
 */
public class FinestraAttivitàCompletate {
    private JButton indietroButton;
    private JPanel attivitàCompletatePanel;

    public JButton getIndietroButton() {
        return indietroButton;
    }

    public void setIndietroButton(JButton indietroButton) {
        this.indietroButton = indietroButton;
    }

    public JPanel getAttivitàCompletatePanel() {
        return attivitàCompletatePanel;
    }

    public void setAttivitàCompletatePanel(JPanel attivitàCompletatePanel) {
        this.attivitàCompletatePanel = attivitàCompletatePanel;
    }
}
