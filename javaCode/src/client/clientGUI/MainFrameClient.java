package client.clientGUI;

import client.UtenteImpl;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mac on 15/07/16.
 */
public class MainFrameClient extends JFrame{

    //Frame contententi i JPanel che si alterneranno sul nostro mainframe

    private FinestraIniziale finestraIniziale = new FinestraIniziale();
    private FinestraLogin finestraLogin = new FinestraLogin();
    private FinestraRegistrazione finestraRegistrazione = new FinestraRegistrazione();
    private FinestraUtenteLoggato finestraUtenteLoggato = new FinestraUtenteLoggato();
    private FinestraGestioneProgetti finestraGestioneProgetti = new FinestraGestioneProgetti();
    private FinestraGestioneAttività finestraGestioneAttività = new FinestraGestioneAttività();
    private FinestraGestioneContatti finestraGestioneContatti = new FinestraGestioneContatti();
    private FinestraNotifiche finestraNotifiche = new FinestraNotifiche();
    private FinestraNuovoProgetto finestraNuovoProgetto = new FinestraNuovoProgetto();
    private FinestraProgettiAttivi finestraProgettiAttivi = new FinestraProgettiAttivi();
    private FinestraProgettiConclusi finestraProgettiConclusi = new FinestraProgettiConclusi();
    private FinestraAttivitàDaSvolgere finestraAttivitàDaSvolgere = new FinestraAttivitàDaSvolgere();
    private FinestraAttivitàCompletate finestraAttivitàCompletate = new FinestraAttivitàCompletate();
    private FinestraVisualizzaContatti finestraVisualizzaContatti = new FinestraVisualizzaContatti();
    private FinestraInviaNotifica finestraInviaNotifica = new FinestraInviaNotifica();
    private FinestraNotificheRicevute finestraNotificheRicevute = new FinestraNotificheRicevute();
    private FinestraNotificheInviate finestraNotificheInviate = new FinestraNotificheInviate();
    private FinestraNuovoContatto finestraNuovoContatto = new FinestraNuovoContatto();


    private CardLayout cardLayout = new CardLayout();

    private JPanel cards = new JPanel(cardLayout);

    //cards per il CardLayout: pannelli che si alternano sul MainFrame dell'applicazione

    private JPanel card1 = finestraIniziale.getStartingPanel();
    private JPanel card2 = finestraLogin.getLoginPanel();
    private JPanel card3 = finestraRegistrazione.getRegistrazionePanel();
    private JPanel card4 = finestraUtenteLoggato.getUtenteLoggatoPanel();
    private JPanel card5 = finestraGestioneProgetti.getGestioneProgettiPanel();
    private JPanel card6 = finestraGestioneAttività.getGestioneAttivitàPanel();
    private JPanel card7 = finestraGestioneContatti.getGestioneContattiPanel();
    private JPanel card8 = finestraNotifiche.getNotifichePanel();
    private JPanel card9 = finestraNuovoProgetto.getNuovoProgettoPanel();
    private JPanel card10 = finestraProgettiAttivi.getProgettiAttiviPanel();
    private JPanel card11 = finestraProgettiConclusi.getProgettiConclusiPanel();
    private JPanel card12 = finestraAttivitàDaSvolgere.getAttivitàDaSvolgerePanel();
    private JPanel card13 = finestraAttivitàCompletate.getAttivitàCompletatePanel();
    private JPanel card14 = finestraVisualizzaContatti.getVisualizzaContattiPanel();
    private JPanel card15 = finestraInviaNotifica.getInviaNotificaPanel();
    private JPanel card16 = finestraNotificheRicevute.getNotificheRicevutePanel();
    private JPanel card17 = finestraNotificheInviate.getNotificheInviatePanel();
    private JPanel card18 = finestraNuovoContatto.getNuovoContattoPanel();


    // ContentPane del Frame
    private Container pane = this.getContentPane();

    // Client associato all'utente connesso
    private UtenteImpl client;


    public MainFrameClient(){

        super("Tetro - Social Planner [Client]");

        //Aggiungo tutti i pannelli al "pannello contenitore" (CardlLayout)

        cards.add(card1, "Starting Page");
        cards.add(card2, "Login");
        cards.add(card3, "Registrazione");
        cards.add(card4, "Logged");
        cards.add(card5, "Gestione Progetti");
        cards.add(card6, "Gestione Attività");
        cards.add(card7, "Gestione Contatti");
        cards.add(card8, "Notifiche");
        cards.add(card9, "Nuovo Progetto");
        cards.add(card10, "Progetti Attivi");
        cards.add(card11, "Progetti Conclusi");
        cards.add(card12, "Attività Da Svolgere");
        cards.add(card13, "Attività Completate");
        cards.add(card14, "Visualizza Contatti");
        cards.add(card15, "Invia Notifica");
        cards.add(card16, "Notifiche Ricevute");
        cards.add(card17, "Notifiche Inviate");
        cards.add(card18, "Nuovo Contatto");



        pane.add(cards);

        cardLayout.show(cards, "Starting Page");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);


        //ActionListeners per il JPanel iniziale

        finestraIniziale.getLoginButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Login");
            }
        });

        finestraIniziale.getRegistratiButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Registrazione");
            }
        });

        //ActionListeners per il JPanel di Login

        finestraLogin.getOKButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String username = finestraLogin.getUsernameField().getText();
                String password = finestraLogin.getPasswordField1().getText();

                //INSERIRE METODO PER IL LOGIN UTENTE

            }
        });

        finestraLogin.getAnnullaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Starting Page");
            }
        });

        //ActionListeners per il JPanel di Registrazione

        finestraRegistrazione.getOKButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String nome = finestraRegistrazione.getNomeField().getText();
                String cognome = finestraRegistrazione.getCognomeField().getText();
                String email = finestraRegistrazione.getEmailField().getText();
                String username = finestraRegistrazione.getUsernameField().getText();
                String password = finestraRegistrazione.getPasswordField().getText();
                String confermaPassword = finestraRegistrazione.getConfermaPasswordField().getText();

                if(!(password.equals(confermaPassword))){
                    JOptionPane.showMessageDialog(null, "Le due password non coincidono!");
                    finestraRegistrazione.getPasswordField().setText("");
                    finestraRegistrazione.getConfermaPasswordField().setText("");
                }
                else if(nome.equals(null) || nome.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Nome non valido");
                }
                else if(cognome.equals(null) || cognome.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Cognome non valido");
                }
                else if(username.equals(null) || username.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Username non valido");
                }
                else{
                    client = new UtenteImpl(nome, cognome, email,  username, password);
                    cardLayout.show(cards, "Logged");
                }

            }
        });

        finestraRegistrazione.getAnnullaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Starting Page");
            }
        });

        //ActionListeners per il JPanel per l'utente loggato

        finestraUtenteLoggato.getGestisciProgettiButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                finestraUtenteLoggato.getBenvenutoLabel().setText("Benvenuto, " + client.getNome() + " Cosa vuoi fare?");
                cardLayout.show(cards, "Gestione Progetti");
            }
        });

        finestraUtenteLoggato.getGestisciAttivitàButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Attività");
            }
        });

        finestraUtenteLoggato.getGestisciContattiButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Contatti");
            }
        });

        finestraUtenteLoggato.getNotificheButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche");
            }
        });

        finestraUtenteLoggato.getLogoutButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards,"Starting Page");
            }
        });

        //ActionListeners per il JPanel di Gestione Progetti

        finestraGestioneProgetti.getCreaNuovoProgettoButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Nuovo Progetto");
            }
        });

        finestraGestioneProgetti.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Logged");
            }
        });

        finestraGestioneProgetti.getProgettiAttiviButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Progetti Attivi");
            }
        });

        finestraGestioneProgetti.getProgettiConclusiButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Progetti Conclusi");
            }
        });


        //ActionListeners per il JPanel di Gestione Attività

        finestraGestioneAttività.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Logged");
            }
        });

        finestraGestioneAttività.getAttivitàDaSvolgereButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Attività Da Svolgere");
            }
        });

        finestraGestioneAttività.getAttivitàCompletateButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Attività Completate");
            }
        });


        //ActionListeners per il JPanel di Gestione Contatti

        finestraGestioneContatti.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Logged");
            }
        });

        finestraGestioneContatti.getVisualizzaContattiButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Visualizza Contatti");
            }
        });

        finestraGestioneContatti.getAggiungiNuovoContattoButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Nuovo Contatto");
            }
        });

        //ActionListeners per il JPanel delle Notifiche

        finestraNotifiche.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Logged");
            }
        });

        finestraNotifiche.getInviaNuovaNotificaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Invia Notifica");
            }
        });

        finestraNotifiche.getVisualizzaNotificheRicevuteButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche Ricevute");
            }
        });

        finestraNotifiche.getVisualizzaNotificheInviateButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche Inviate");
            }
        });


        //ActionListeners per il JPanel di creazione Nuovo Progetto

        finestraNuovoProgetto.getAnnullaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Progetti");
            }
        });

        finestraNuovoProgetto.getCreaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String titolo = finestraNuovoProgetto.getTitoloField().getText();
                String descrizione = finestraNuovoProgetto.getDescrizioneArea().getText();
                //Leggere e salvare in una variabile la data di scadenza

                //Inserire codice per creazione nuovo progetto;
            }
        });

        //Action Listeners per il JPanel di visualizzazione Progetti Attivi

        finestraProgettiAttivi.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Progetti");

                //Aggiungere un evento per i pulsanti di modifica progetto
            }
        });

        //Action Listeners per il JPanel di visualizzazione Progetti Conclusi

        finestraProgettiConclusi.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Progetti");
            }
        });

        //Action Listeners per il JPanel di visualizzazione Attività Da Svolgere

        finestraAttivitàDaSvolgere.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Attività");
            }
        });

        //Action Listeners per il JPanel di visualizzazione Attività Completate

        finestraAttivitàCompletate.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Attività");
            }
        });

        //Action Listeners per il JPanel di visualizzazione Contatti

        finestraVisualizzaContatti.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Gestione Contatti");
            }
        });

        //Action Listeners per il JPanel di creazione nuovo contatto

        finestraNuovoContatto.getAnnullaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards,"Gestione Contatti");
            }
        });

        finestraNuovoContatto.getCreaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String nome = finestraNuovoContatto.getNomeField().getText();
                String cognome = finestraNuovoContatto.getCognomeField().getText();
                String email = finestraNuovoContatto.getEmailField().getText();

                if(nome.equals(null) || nome.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Nome non valido");
                }
                else if(cognome.equals(null) || cognome.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Cognome non valido");
                }

            }
        });

        //ActionListener per il JPanel di visualizzazione Notifiche Ricevute

        finestraNotificheRicevute.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche");
            }
        });

        //ActionListener per il JPanel di visualizzazione Notifiche Inviate

        finestraNotificheInviate.getIndietroButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche");
            }
        });

        //ActionListener per il JPanel di creazione e invio Nuova Notifica

        finestraInviaNotifica.getAnnullaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(cards, "Notifiche");
            }
        });

        finestraInviaNotifica.getInviaButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //inserire chiamata a metodo per la creazione e l'invio della notifica
            }
        });

    }

    public static void main(String[] args) {

        MainFrameClient mainFrameClient = new MainFrameClient();

    }
}
