package client.clientGUI;

import javax.swing.*;

/**
 * Created by mac on 19/07/16.
 */
public class FinestraNotificheRicevute {

    private JPanel visualizzaNotifichePanel;
    private JPanel notificheRicevutePanel;
    private JPanel notificheInviatePanel;
    private JList listaNotificheRicevute;
    private JList listaNotificheInviate;
    private JButton indietroButton;

    public JPanel getVisualizzaNotifichePanel() {
        return visualizzaNotifichePanel;
    }

    public void setVisualizzaNotifichePanel(JPanel visualizzaNotifichePanel) {
        this.visualizzaNotifichePanel = visualizzaNotifichePanel;
    }

    public JPanel getNotificheRicevutePanel() {
        return notificheRicevutePanel;
    }

    public void setNotificheRicevutePanel(JPanel notificheRicevutePanel) {
        this.notificheRicevutePanel = notificheRicevutePanel;
    }

    public JPanel getNotificheInviatePanel() {
        return notificheInviatePanel;
    }

    public void setNotificheInviatePanel(JPanel notificheInviatePanel) {
        this.notificheInviatePanel = notificheInviatePanel;
    }

    public JList getListaNotificheRicevute() {
        return listaNotificheRicevute;
    }

    public void setListaNotificheRicevute(JList listaNotificheRicevute) {
        this.listaNotificheRicevute = listaNotificheRicevute;
    }

    public JList getListaNotificheInviate() {
        return listaNotificheInviate;
    }

    public void setListaNotificheInviate(JList listaNotificheInviate) {
        this.listaNotificheInviate = listaNotificheInviate;
    }

    public JButton getIndietroButton() {
        return indietroButton;
    }

    public void setIndietroButton(JButton indietroButton) {
        this.indietroButton = indietroButton;
    }
}
