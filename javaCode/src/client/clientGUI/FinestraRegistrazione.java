package client.clientGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mac on 12/07/16.
 */
public class FinestraRegistrazione extends Container {

    private JPanel registrazionePanel;
    private JTextField cognomeField;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JTextField nomeField;
    private JButton OKButton;
    private JButton annullaButton;
    private JLabel inserisciITuoiDatiLabel;
    private JLabel cognomeLabel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JLabel nomeLabel;
    private JLabel confermaPasswordLabel;
    private JPasswordField confermaPasswordField;
    private JTextField emailField;

    public JPanel getRegistrazionePanel() {
        return registrazionePanel;
    }

    public void setRegistrazionePanel(JPanel registrazionePanel) {
        this.registrazionePanel = registrazionePanel;
    }

    public JTextField getCognomeField() {
        return cognomeField;
    }

    public void setCognomeField(JTextField cognomeField) {
        this.cognomeField = cognomeField;
    }

    public JTextField getUsernameField() {
        return usernameField;
    }

    public void setUsernameField(JTextField usernameField) {
        this.usernameField = usernameField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(JPasswordField passwordField) {
        this.passwordField = passwordField;
    }

    public JTextField getNomeField() {
        return nomeField;
    }

    public void setNomeField(JTextField nomeField) {
        this.nomeField = nomeField;
    }

    public JButton getOKButton() {
        return OKButton;
    }

    public void setOKButton(JButton OKButton) {
        this.OKButton = OKButton;
    }

    public JButton getAnnullaButton() {
        return annullaButton;
    }

    public void setAnnullaButton(JButton annullaButton) {
        this.annullaButton = annullaButton;
    }

    public JLabel getInserisciITuoiDatiLabel() {
        return inserisciITuoiDatiLabel;
    }

    public void setInserisciITuoiDatiLabel(JLabel inserisciITuoiDatiLabel) {
        this.inserisciITuoiDatiLabel = inserisciITuoiDatiLabel;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public void setEmailField(JTextField emailField) {
        this.emailField = emailField;
    }

    public JLabel getCognomeLabel() {
        return cognomeLabel;
    }

    public void setCognomeLabel(JLabel cognomeLabel) {
        this.cognomeLabel = cognomeLabel;
    }

    public JLabel getUsernameLabel() {
        return usernameLabel;
    }

    public void setUsernameLabel(JLabel usernameLabel) {
        this.usernameLabel = usernameLabel;
    }

    public JLabel getPasswordLabel() {
        return passwordLabel;
    }

    public void setPasswordLabel(JLabel passwordLabel) {
        this.passwordLabel = passwordLabel;
    }

    public JLabel getNomeLabel() {
        return nomeLabel;
    }

    public void setNomeLabel(JLabel nomeLabel) {
        this.nomeLabel = nomeLabel;
    }

    public JLabel getConfermaPasswordLabel() {
        return confermaPasswordLabel;
    }

    public void setConfermaPasswordLabel(JLabel confermaPasswordLabel) {
        this.confermaPasswordLabel = confermaPasswordLabel;
    }

    public JPasswordField getConfermaPasswordField() {
        return confermaPasswordField;
    }

    public void setConfermaPasswordField(JPasswordField confermaPasswordField) {
        this.confermaPasswordField = confermaPasswordField;
    }
}

