package notifiche;

import utenti.Utente;

import java.util.ArrayList;

/**
 * Created by mac on 27/05/16.
 */
public class Notifica {

    private String messaggio;
    private ArrayList<Utente> Destinatari;
    private tipologia tipoMessaggio;


    private enum tipologia{

        SVOLTO,
        NON_SVOLTO,
        GENERICO;


    }




    public void Notifica(String messaggio, ArrayList<Utente> Destinatari, tipologia tipoMessaggio){

        messaggio = this.messaggio;
        Destinatari = this.Destinatari;
        tipoMessaggio = this.tipoMessaggio;

    }


    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public ArrayList<Utente> getDestinatari() {
        return Destinatari;
    }

    public void setDestinatari(ArrayList<Utente> destinatari) {
        Destinatari = destinatari;
    }


}
