package common;

import sistemaCentrale.Progetto;
import sistemaCentrale.Team;
import utenti.Utente;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by giuseppetesta on 27/05/16.
 */
public interface TeamInterface extends Remote {

    public void addMember(Progetto myProg, Utente utente) throws RemoteException;

    public void removeMemeber(Progetto myProg, Utente utente) throws RemoteException;

    public Team getTeam(Progetto myProg) throws RemoteException;


    }
