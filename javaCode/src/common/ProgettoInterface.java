package common;

import sistemaCentrale.Progetto;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

/**
 * Created by giuseppetesta on 27/05/16.
 */
public interface ProgettoInterface extends Remote {

    void modificaNome(Progetto myProject, String newName) throws RemoteException;
    void modificaScadenza(Progetto myProject, Date newDate) throws RemoteException;
    void modificaStatoPorgetto(Progetto p, Progetto.StatoProgetto newStato) throws RemoteException;

    void chiudiProgetto(Progetto myProject) throws RemoteException;

    //TaskIteratorImpl creaIterator();


}
