package common;

import sistemaCentrale.Progetto;
import sistemaCentrale.Task;
import sistemaCentrale.Team;
import utenti.Responsabile;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by giuseppetesta on 04/06/16.
 */
public interface MyFactory extends Remote{

    Progetto creaProgetto(Utente utente, String nome, String descrizione) throws RemoteException;
    Task creaTask(ArrayList<Utente> responsabili, String nome, String descrizione, String luogo) throws RemoteException;
}
