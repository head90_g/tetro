package common;

import sistemaCentrale.Progetto;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by giuseppetesta on 07/06/16.
 */
public interface Server extends Remote{

    void registraUtente(Utente utente) throws RemoteException;

    void addProgetto(Progetto progetto) throws RemoteException;

    void chiudiProgetto(Progetto progetto) throws RemoteException;

    void visualizzaProgetto(Progetto progetto) throws RemoteException;

    void stampaProgettiAttivi() throws RemoteException;

    void stampaUtentOnline() throws RemoteException;

    void stampaProgettiConclusi() throws RemoteException;

    void unregisterClient(Utente client) throws RemoteException;

    ArrayList<Progetto> getProgetti() throws RemoteException;

}
