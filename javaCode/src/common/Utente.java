package common;

import sistemaCentrale.Progetto;
import sistemaCentrale.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author Giuseppe Testa
 *         06/06/16
 */

public interface Utente extends Remote {

    void login()  throws RemoteException;
    void logout() throws RemoteException;

    boolean isLogin() throws RemoteException;
    boolean setLogin(boolean log) throws RemoteException;

    void addTask(Progetto progetto, Task task) throws RemoteException;

    void addProgetto(Progetto progetto) throws RemoteException;

    void gestisciProgetto(Progetto progetto, Utente utente) throws RemoteException;

    ArrayList<Progetto> getProjects() throws RemoteException;
    void visualizzaProgetto(Progetto progetto) throws RemoteException;

    String getNome() throws RemoteException;
    void setNome(String nome) throws RemoteException;

}
