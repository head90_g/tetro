package common;

import sistemaCentrale.Progetto;
import sistemaCentrale.Task;

/**
 * Created by giuseppetesta on 01/06/16.
 */
public interface TaskIterator {

    boolean hasNext(Progetto p);

    Task next();
}
