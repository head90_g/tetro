package common;

import sistemaCentrale.Progetto;
import sistemaCentrale.Task;
import utenti.Responsabile;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

/**
 * Created by giuseppetesta on 27/05/16.
 */

public interface TaskInterface extends Remote {

    void removeTask(Progetto progetto, int pos) throws RemoteException;
    void visualizzaTask(Progetto p) throws RemoteException;
    void addTask(Task task, Progetto progetto) throws RemoteException;

    void modificaNomeTask(Progetto p, Task t, String newName) throws RemoteException;
    void modificaDescrizioneTask(Progetto p, Task t, String newDescrizione) throws RemoteException;
    void modificaLuogoTask(Progetto p , Task t, String newLuogo) throws RemoteException;
    void modificaDeadlineTask(Progetto p , Task t, Date newDeadline) throws RemoteException;
    void modificaStatoTask(Progetto p, Task t, Task.StatoTask newStato) throws RemoteException;
    void modificaOrdineTask(Progetto p, Task t, int ordine) throws RemoteException;

    void addResponsabile(Progetto p, Task t, Responsabile r) throws RemoteException;
    void removeResponsabile(Progetto p, Task t, Responsabile r) throws RemoteException;

    }
